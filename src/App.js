import React, { Component } from 'react';
import './App.css';
import Search from './components/Search';


class App extends Component {
  render() {
    return (
      <div className="bg-img">
        <Search />
      </div>
    );
  }
}

export default App;
