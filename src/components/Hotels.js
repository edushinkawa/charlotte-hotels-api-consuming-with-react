import React, { Component } from 'react';

import '../App.css'

import star from '../images/star.svg'

import { observer, inject } from "mobx-react"
import StarRatingComponent from 'react-star-rating-component'

class Hotels extends Component {
  render() {    
    const { hotels, min, max, rating, history, hotelsObj } = this.props.Main 
    const hotelCard = {}
    const ViewHistoryButton = (props) =>
    <button className= "viewHistorybutton"  onClick={() => this.props.Main.goToHistory(props)}>
        Price History
    </button>
    const BookNowButton = () =>
    <button className= "bookNowButton"disabled>
        Book now
    </button>

    const Card = props => 
      <section>
        <div className="cardResult">
          <div className="img" style={{ backgroundImage: `url(${props.props.image})` }}></div>
            <div className="desc">
              <div>
                <StarRatingComponent 
                name="rate1" 
                starCount={props.props.rate}
                value={props.props.rate}
                />
                <p className="descTitle">{props.props.name}</p>
                <p className="descPar">{props.props.description}</p>
                <div className="buttonsDescContainer">
                  <BookNowButton />
                  <ViewHistoryButton />
                </div>
              </div>
          </div>
          <div className="total">
            <p>Total</p>
            <p>${props.props.price && props.props.price.toFixed(2)}</p>
          </div>
        </div>
      </section>

    return (  
        <section className="hotelsList"> 
          {Object.keys(hotelsObj)
            .filter(key => hotelsObj[key].rate >= rating && hotelsObj[key].price >= min && hotelsObj[key].price <= max )
            .map((hotel, key) => <Card props={hotelsObj[key]} />)}      
        </section>    
    );
  }
}

export default inject("Main")(observer(Hotels));