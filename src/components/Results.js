import React, { Component } from 'react'

import '../App.css'
import 'react-day-picker/lib/style.css'
import 'rc-slider/assets/index.css';

import Hotels from './Hotels'

import moment from "moment"
import DayPicker, { DateUtils } from 'react-day-picker'
import { observer, inject } from "mobx-react"
import Slider, { Range } from 'rc-slider'
import StarRatingComponent from 'react-star-rating-component'

class Results extends Component {
  constructor(props) {
    super(props)
    this.onSliderChange = this.onSliderChange.bind(this)
    this.state = {
      value: [0, 100],
      min: 0,
      max: 100,
    }
  }

  onStarClick = (nextValue, prevValue, name) => {
    this.props.Main.rating = nextValue
  }
  
  onSliderChange = (value) => {
    console.log(value)
    this.setState({
      value,
    })
    this.props.Main.min = value[0] * 5 + 100
    this.props.Main.max = value[1] * 5 + 100
    this.props.Main.value = value
  }

  render() {
    const { from, to, rating, min, max, history } = this.props.Main 
    const formatFrom = from && <span>{moment(from).format('MM/DD/YYYY')}</span>
    const formatTo = to && <span>{moment(to).format('MM/DD/YYYY')}</span>
    const markStyles = {
      parag : { fontSize: 10, color: '#b5b5b5', marginTop: 8, marginLeft: 20 },
      minParag: {
        marginLeft: 48,
        fontSize: 19,
        marginTop: '-12px',
        color: 'lightsalmon',
        left: '0'
      },
      maxParag: {
        marginLeft:'-5px',
        fontSize: 19,
        marginTop: '-12px',
        color: 'lightsalmon'
      },
      hrStyle: {
        marginTop:35,
        width: '111%',
        marginLeft: '-5px'
      },
      priceRange: {
        fontSize: 11,
        color: '#b5b5b5',
        letterSpacing: 1,
        marginTop: 30
      },
      price: {
        fontSize: 15,
        marginTop: 10
      }
    }
    const marks = {
      100: <div><p style={markStyles.parag}>Min</p><p style={markStyles.minParag}>$100</p></div>,
      600: <div><p style={markStyles.parag}>Max</p><p style={markStyles.maxParag}>$600</p></div>
    }
    
    const InputRange = () =>
    <div>
      <p>Filter</p>
      <p style={markStyles.priceRange}>Prince Range</p>
      <Range 
        allowCross={false} 
        //marks={marks}
        value={this.state.value}
        trackStyle={[{ backgroundColor: 'orange' }, { backgroundColor: 'orange' }]}   
        onChange={this.onSliderChange}     
        handleStyle={[
        { 
          backgroundColor: 'white', 
          height: 28,
          width: 28, 
          borderColor: 'orange',
          marginTop: '-12px',
          border: '4px solid orange'
        }, 
        { 
          backgroundColor: 'white', 
          borderColor: 'orange', 
          height: 28,
          width: 28,
          marginTop: '-12px',
          border: '4px solid orange'
        }
      ]}      
      />  
      <p style={markStyles.price}>From ${this.props.Main.min} to ${this.props.Main.max}</p>
      <div><hr style={markStyles.hrStyle}/></div>
    </div>

    const Stars = () =>
    <div>
      <p style={markStyles.priceRange}>Stars</p>
      <StarRatingComponent 
          name="rate1" 
          starCount={5}
          value={rating}
          onStarClick={this.onStarClick.bind(this)}
      />
    </div>
    
    
    return (
      <section className="resultsSection container">
        <h3 className="title-section">Best choices between {formatFrom} and {formatTo}</h3>
        <div className="row">
            <div className="col-md-3 title-section text-left">      
              <InputRange />
              <Stars />
            </div> 
            <div className="offset-md-1 col-md-8">
              <Hotels />
            </div>            
        </div>
      </section>
    );
  }
}

export default inject("Main")(observer(Results));