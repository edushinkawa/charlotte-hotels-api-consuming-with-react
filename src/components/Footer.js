import React, { Component } from 'react';
import '../App.css';
import instagram from '../images/instagram.svg';
import facebook from '../images/facebook.svg';
import twitter from '../images/twitter.svg';

class Footer extends Component {
  render() {    
    const styleSvg = { maxWidth : '4%' }
    const social = [instagram, facebook, twitter]
    return (  
        <header className="socialFooter"> 
          {social.map((item, index) => 
            <img src={item} key={index} style={styleSvg} />
          )}
          <p>© 2004-2017 Visit Charlotte. All Rights Reserved. 500 S. College Street, Suite 300, Charlotte, NC 28202</p>           
        </header>    
    );
  }
}

export default Footer;
