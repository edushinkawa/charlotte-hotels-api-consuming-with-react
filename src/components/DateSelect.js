import React, { Component } from 'react'

import '../App.css'
import 'react-day-picker/lib/style.css'

import moment from "moment"
import DayPicker, { DateUtils } from 'react-day-picker'
import { observer, inject } from "mobx-react"

class DateSelect extends Component {

    state = {
        from: null,
        to: null,
    }
    
    handleDayClick = day => {
        const range = DateUtils.addDayToRange(day, this.state)
        this.setState(range, function(){
            this.props.Main.from = this.state.from
            this.props.Main.to = this.state.to        
        })
    }

  render() {
    const styleChoose = { fontSize : 15 }
    const { from, to } = this.props.Main
    const Checks = () => 
        <section className="checks">
            <div>
                <p>CHECK-IN</p>
                {from ? <p>{moment(from).format('MMMM')} <span>{moment(from).format('DD')}</span>, {moment(from).format('YYYY')}</p> : <p style={styleChoose}>Choose a date</p>}
            </div>
            <div>
                <p>CHECK-OUT</p>
                {to ? <p>{moment(to).format('MMMM')} <span>{moment(to).format('DD')}</span>, {moment(to).format('YYYY')}</p> : <p style={styleChoose}>Choose a date</p>}
            </div>
        </section>
    
    const ButtonDisabled = () =>
        <button className= "searchHotelButtonDisabled" disabled>
            Search hotels
        </button>
    const Button = () =>
        <button className="searchHotelButtonEnabled"             
                onClick={() => this.props.Main.search()}        
        >
            Search hotels
        </button>
    
    return (
      <section className="dateSelectSection">
        <h3 className="title-section">Select the dates to stay in Charlotte</h3>
        <div className="dateComponents">
            <div>
                <Checks />
                {!from || !to ?  <ButtonDisabled /> :  <Button />}
            </div>

            <div>
            <DayPicker
                numberOfMonths={1}
                selectedDays={[from, { from, to }]}
                onDayClick={this.handleDayClick}
                fixedWeeks
                />
            </div>
        </div>
      </section>
    );
  }
}

export default inject("Main")(observer(DateSelect));