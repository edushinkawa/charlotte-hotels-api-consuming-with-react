import React, { Component } from 'react'
import '../App.css'
import DateSelect from './DateSelect'
import Results from './Results'
import Footer from './Footer'
import crown from '../images/crown.svg'
import { observer, inject } from "mobx-react"

class Search extends Component {

  render() {
    const { from, to, hotels } = this.props.Main        
    const menuItems = ["The Queen city", "My Reservations", "Guide"]
    const Menu = () => 
      <header className="searchMenuItems"> 
        {menuItems.map((item, index) => <li key={index}>{item}</li>)}
      </header>
    
    const WelcomeSection = () => 
      <section className="welcomeSection"> 
        <img src={crown} style={{maxWidth : '4%'}}/>
        <div>
          <p>WELCOME TO</p>
          <p><span>CHARLOTTE</span></p>
          <p>THE QUEEN CITY</p>
        </div>
      </section>

    return (
      <div className="bg-img">
          <Menu />
          <WelcomeSection />
          <DateSelect />
          {hotels.length > 0 && <Results />}
          <Footer />
      </div>
    );
  }
}

export default inject("Main")(observer(Search));