import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import Main from "./Stores/Main"
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './index.css';

import registerServiceWorker from './registerServiceWorker';

import { Provider } from "mobx-react"

const stores = { Main }

ReactDOM.render(
    <Provider {...stores}>
        <App />
    </Provider>, 
    document.getElementById('root'));
    registerServiceWorker();
