import { extendObservable, computed, action, autorun } from "mobx";
import axios from "axios";

class Main {
  constructor() {
    extendObservable(this, {
      to: null,
      from: null,
      hotels: [],
      min: 100,
      max: 600,
      rating: 1,
      history: false,
      hotelsObj: {}
    })
  }

  search() {
    let self = this
    this.albums = []
    this.loading = true
    axios
      .get("http://www.raphaelfabeni.com.br/rv/hotels.json")
      .then(function(response) {
        self.hotels = response.data.hotels
        self.hotelsObj = Object.assign({}, response.data.hotels)
    })
      .catch(function(error) {
        self.loading = false
      })
  }
  
  onSliderChange = (value) => {
    this.value = value
    this.min = value[0]
    this.max = value[1]
  }

  onChangeInput(e) {
    this.searchParam = e.target.value;
  }

}

export default new Main();
